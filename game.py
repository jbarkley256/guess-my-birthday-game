from random import randint

name = input("Hi! What is your name?")
max_guess = 5

for guess_num in range(max_guess):
    mon_guess = randint(1, 12)
    year_guess = randint(1924, 2004)

    print("Guess", guess_num + 1, ":", name, "were you born in", mon_guess, "/", year_guess)
    answer = input("yes or no?")
    if answer == "no":
        print("Drat! Lemme try again!")

    elif answer == "yes":
        print("I knew it!")
        exit()
    else:
        print("Invalid input. Please try again.")

print ("I have other things to do. Good bye.")
